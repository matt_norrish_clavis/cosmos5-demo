# Cosmos demo

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It also uses [React Cosmos 5](https://github.com/react-cosmos/react-cosmos/blob/master/NEXT.md) as a component library and development sandbox. This version of Cosmos is an alpha release so it isn't quite complete.

The main `App` component has been split into smaller components to better demonstrate how Cosmos fixures work.
These components are in the `src/components` directory.
Styles have also been converted to CSS modules.

Cosmos uses fixtures which are defined in files suffixed with `.fixture.js`. These are co-located with component modules.

A `Counter` component has also been added to demonstrate [Redux](https://redux.js.org/) state management and how Redux state can be mocked in fixtures.

## Getting started

Make sure to have Node.js installed (_Tested with Node.js v10.15 and NPM v6.4_).

Clone this repository to your workstation.

From the repository's root directory run `npm i` to install dependencies.

Run `npm start` to run the app. It will automatically open in your browser.

Run `npm run cosmos` to run Storybook. It will run at [http://localhost:8989](http://localhost:8989).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run cosmos`

Runs the component library in the development mode.<br>
Open [http://localhost:8989](http://localhost:8989) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
