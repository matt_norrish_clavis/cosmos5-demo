import React from 'react';
import Button from './Button';

// Let's log clicks to the console
const onClick = event => console.log('button-click', event);

export default <Button onClick={onClick}>Sample text</Button>;
