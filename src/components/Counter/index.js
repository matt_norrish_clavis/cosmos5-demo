import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux'
import Button from './Button';
import classes from './index.module.css';

const Counter = ({ showReset, children }) => {
    const count = useSelector(state => state.count);
    const dispatch = useDispatch();
    const decrement = () => dispatch({ type: 'COUNT_DECREMENT' });
    const increment = () => dispatch({ type: 'COUNT_INCREMENT' });
    const reset = () => dispatch({ type: 'COUNT_RESET' });
    
    return (
        <div className={classes.counter}>
            <Button onClick={decrement}>-</Button>
            {children(count)}
            <Button onClick={increment}>+</Button>
            {showReset && <Button onClick={reset}>Reset</Button>}
        </div>
    );
};

Counter.propTypes = {
    showReset: PropTypes.bool,
    children: PropTypes.func,
};
Counter.defaultProps = {
    showReset: false,
    children: count => count,
};

export default Counter;
