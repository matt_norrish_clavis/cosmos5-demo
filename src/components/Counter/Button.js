import React from 'react';
import PropTypes from 'prop-types';
import classes from './index.module.css';

const Button = ({ children, onClick }) => (
    <div role="button" onClick={onClick} className={classes.button}>
        {children}
    </div>
);

Button.propTypes = {
    children: PropTypes.node,
    onClick: PropTypes.func,
};
Button.defaultProps = {
    children: null,
    onClick: null,
};

export default Button;
