import React from 'react';
import ReduxState from '../../ReduxStateMock';
import Counter from '.';

// These fixtures use mocked Redux state
export default {
    'Default props': (
        <ReduxState>
            <Counter />
        </ReduxState>
    ),
    'Show reset': (
        <ReduxState>
            <Counter showReset />
        </ReduxState>
    ),
    'With render prop': (
        <ReduxState>
            <Counter>
                {count => <>The count is {count}</>}
            </Counter>
        </ReduxState>
    ),
    'With count of 50': (
        <ReduxState initialState={{ count: 50 }}>
            <Counter />
        </ReduxState>
    ),
};
