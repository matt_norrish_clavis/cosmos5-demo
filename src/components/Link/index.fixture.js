import React from 'react';
import Link from '.';

export default {
    'Default props': <Link href="#">Sample text</Link>,
    'External link': <Link href="https://github.com" target="_blank">Github</Link>,
};
