import React from 'react';
import Header from '.';

export default {
    'Default props': <Header />,
    'With content': <Header><p>Hello, World!</p><p>I'm in the Header!</p></Header>,
};
