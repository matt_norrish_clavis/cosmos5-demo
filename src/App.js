import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Header from './components/Header';
import Logo from './components/Logo';
import Link from './components/Link';
import classes from './App.module.css';
import Counter from './components/Counter';

function App() {
  return (
    <Provider store={store}>
      <div className={classes.App}>
        <Header>
          <Logo />
          <p>This sample app is based on Create React App.</p>
          <Link href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
            Learn React
          </Link>
          <p>It also demonstrates very simple use of Redux.</p>
          <Counter showReset />
        </Header>
      </div>
    </Provider>
  );
}

export default App;
