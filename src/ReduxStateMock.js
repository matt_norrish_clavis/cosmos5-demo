import React from 'react';
import PropTypes from 'prop-types';
import { ReduxMock } from 'react-cosmos-redux';
import { createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import reducer from './reducer';
import { INITIAL_STATE } from './store';

const configureStore = state => createStore(reducer, state, devToolsEnhancer({ name: 'Cosmos' }));

const ReduxStateMock = ({ children, initialState }) => (
    <ReduxMock configureStore={configureStore} initialState={initialState}>
        {children}
    </ReduxMock>
);

ReduxStateMock.propTypes = {
    children: PropTypes.node,
    initialState: PropTypes.any,
};
ReduxStateMock.defaultProps = {
    children: null,
    initialState: INITIAL_STATE,
};

export default ReduxStateMock;
