import './index.css';

// This is a Cosmos 'decorator'
// A decorator is a component which allows you to wrap fixtures with other components.
// It takes only a children prop which is the fixture.
// This is helpful when an application uses context providers.
// Cosmos automatically uses decorators in order of the project's directory tree hierarchy.
export default ({ children }) => children;
